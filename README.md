## Data Analysis and Interpretation
is a [coursera specialisation](https://www.coursera.org/specializations/data-analysis) I'm following. It teaches the basics of data analysis in Python (or SAS), which I found intriguing enough to give it a try. In addition to Python, I'm using R to do (more or less) the same analysis steps and compare the two languages.  
The results are actually published and discussed on my [tumblr](http://lilithelina.tumblr.com/), but are better readable with the GitLab infrastructure behind them. This is why you can find the IPython/Jupyter Notebooks, RMarkdown files and their respective HTML and Markdown result files here. In case you are not familiar with the possibilities, you can nicely display .ipynb files with the [nbviewer](http://nbviewer.ipython.org/) or those and the equivalent Markdown files directly in this repository (see links below).

The specialisation consists of four courses: [Data Management and Visualization](https://www.coursera.org/learn/data-visualization), [Data Analysis Tools](https://www.coursera.org/learn/data-analysis-tools), [Regression Modeling in Practice](https://www.coursera.org/learn/regression-modeling-practice), [Machine Learning for Data Analysis](https://www.coursera.org/learn/machine-learning-data-analysis), and a capstone ([Data Analysis and Interpretation Capstone](https://www.coursera.org/learn/data-analysis-capstone)).  
Below, you can find a list of the courses and their weekly assignments I have already completed and uploaded here:

**Data Management and Visualisation** ([DataManViz](DataManViz/))

* [Selecting a research question](DataManViz/Week%20One.md)
* Writing your first program ([Python](DataManViz/Week%20Two.md) | [R](DataManViz/Week_Two.md))
* Managing data ([Python](DataManViz/Week%20Three.md) | [R](DataManViz/Week_Three.md))
* Visualising data ([Python](DataManViz/Week%20Four.md) | [R](DataManViz/Week_Four.md))

**Data Analysis Tools** ([DataAnaT](DataAnaT))

* Hypothesis testing and ANOVA ([Python](DataAnaT/Week%20One%20-%20ANOVA.md) | [R](DataAnaT/Week_One_ANOVA.md))
* Chi square test of independence ([Python](DataAnaT/Week%20Two%20-%20Chi-Square.md) | [R](DataAnaT/Week_Two_Chi.md))
* Pearson correlation ([Python](DataAnaT/Week%20Three%20-%20Pearson%20Correlation.md) | [R](DataAnaT/Week_Three_Pearson.md))
* Exploring statistical interactions ([Python](DataAnaT/Week%20Four%20-%20Causation.md) | [R](DataAnaT/Week_Four_Moderator.md))

**Regression Modeling in Practice** ([RegModPrac](RegModPrac))

* [Introduction to regression](RegModPrac/Week%20One.md)
* Basics of linear regression ([Python](RegModPrac/Week%20Two%20-%20Basic%20Linear%20Regression.md) | [R](RegModPrac/Week_Two_BasicRegression.md))
* Multiple regression ([Python](RegModPrac/Week%20Three%20-%20Polynomial%20Regression.md) | [R](RegModPrac/Week_Three_PolynomialRegression.md))
* Logistic regression ([Python](RegModPrac/Week%20Four%20-%20Logistic%20Regression.md) | R)
